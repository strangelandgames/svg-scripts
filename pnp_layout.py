#!/usr/bin/env python
import sys
import os
from xml.etree import ElementTree as et
import argparse
from subprocess import call

# This script assumes that every image in the deck directory is a card and that it has a corresponding image
# in the back directory for the card back. It then lays them out at standard card sizes on 8.5 x 11 svg's to be printed duplex

# cards are 63.5 x 88 mm
# paper is 216 x 279 mm
# therefore you can fit three cards wide by three cards tall with 6 mm horizontal spacing and 3 mm vertical spacing
# Take the deck and back directories as inputs
xes = ['6.35mm', '76.2mm', '146.05mm']
yes = ['3.85mm', '95.7mm', '187.55mm']

parser = argparse.ArgumentParser(description="Build PnP files for decks of cards")
parser.add_argument('face_directory',nargs='?',type=str,help='The directory where the card faces are stored')
parser.add_argument('back_location',nargs='?',type=str,help='The directory storing the card backs')
parser.add_argument('--singleback',action='store_true',help='This flag changes the interpretation of back_location from directory to file')
args = parser.parse_args()

cards = os.listdir(args.face_directory)
print cards

# create an SVG XML element (see the SVG specification for attribute details)
xmlns = {'xmlns:dc' : 'http://purl.org/dc/elements/1.1/',
         'xmlns:cc' : "http://creativecommons.org/ns#",
         'xmlns:rdf' : "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
         'xmlns:svg' : "http://www.w3.org/2000/svg",
         'xmlns' : "http://www.w3.org/2000/svg",
         'xmlns:xlink' : "http://www.w3.org/1999/xlink"}
svgheader = '<?xml version=\"1.0\" standalone=\"no\"?>\n' + '<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n' + '\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n'
pagecount = 0
x = 0
y = 0
for cardname in cards:
    if (x==0 and y==0):
        front = et.Element('svg', xmlns, width='216mm', height='279mm', version='1.1')
        back = et.Element('svg', xmlns, width='216mm', height='279mm', version='1.1')
        for page in {front, back}:
            et.SubElement(page, 'circle', cx='1mm', cy='1mm', r='1mm', color='#000000')
            et.SubElement(page, 'circle', cx='215mm', cy='278mm', r='1mm', color='#000000')
    
    # add a circle (using the SubElement function)
    frontimg = {'xlink:href' : args.face_directory + cardname}
    if(args.singleback) : backimg = {'xlink:href' : args.back_location}
    else: backimg = {'xlink:href' : args.back_location + cardname}
    et.SubElement(front, 'image', frontimg, x=xes[x], y=yes[y], width='63.5mm', height='88mm')
    et.SubElement(back, 'image', backimg, x=xes[2-x], y=yes[y], width='63.5mm', height='88mm')

    x+=1
    if(x==3):
        x = 0
        y+= 1
    if(y==3 or cardname==cards[len(cards)-1]):
        # ElementTree 1.2 doesn't write the SVG file header errata, so do that manually
        f = open('front'+str(pagecount)+'.svg', 'w')
        b = open('back'+str(pagecount)+'.svg', 'w')
        f.write(svgheader)
        f.write(et.tostring(front))
        f.close()
        b.write(svgheader)
        b.write(et.tostring(back))
        b.close()
        call(['mogrify', '-density', '300', '-format', 'pdf', 'front'+str(pagecount)+'.svg'])
        call(['mogrify', '-density', '300', '-format', 'pdf', 'back'+str(pagecount)+'.svg'])
        y=0
        pagecount+=1

allfiles = ['pdftk']
for p in range(0,(pagecount)):
    allfiles.append('front'+str(p)+'.pdf')
    allfiles.append('back'+str(p)+'.pdf')
allfiles.extend(['cat', 'output', 'deck.pdf', 'compress'])
call(allfiles)
